import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginModule } from 'dimple0812-login';
import { SignupModule} from '@dimple0812/signup'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginAppComponent } from './components/login-app/login-app.component';
import { HomeComponent } from './components/home/home.component';
import { SignupAppComponent } from './components/signup-app/signup-app.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginAppComponent,
    HomeComponent,
    SignupAppComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    SignupModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
