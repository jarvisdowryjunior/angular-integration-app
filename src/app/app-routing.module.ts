import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginAppComponent } from './components/login-app/login-app.component';
import { HomeComponent } from './components/home/home.component';
import { SignupAppComponent } from './components/signup-app/signup-app.component';


const routes: Routes = [
  { path:'' , component: HomeComponent},
  { path:'login' , component: LoginAppComponent },
  { path:'signup' , component: SignupAppComponent}
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
