import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup-app',
  //templateUrl: './signup-app.component.html',
  template: `<lib-signup></lib-signup>`,
  styleUrls: ['./signup-app.component.css']
})
export class SignupAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
