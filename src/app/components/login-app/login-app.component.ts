import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-login-app',
  //templateUrl: './login-app.component.html',
  template: `<lib-login></lib-login>`,
  styleUrls: ['./login-app.component.css']
})
export class LoginAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
