import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public router:Router){}
  title = 'angular-integration-app';

  onLoginClick():void{
    console.log("login button clicked");
    this.router.navigate(['/login/']);
  }

  onSignupClick():void{
    console.log("Signup button clicked");
    this.router.navigate(['/signup/']);
  }

  ngOnInit() {
  }

}
